INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");


//Taylor Swift

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 3);


INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 246, "Pop rock", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", 213, "Country pop", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 243, "Rock, alternative rock, arena rock", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 204, "Country", 4);


//Lady Gaga

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-10-10", 4);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-6-29", 4);


INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 5);	
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 6);


//Justin Bieber

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-11-13", 5);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012-6-15", 5);


INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 132, "Dancehall-poptropical housemoombahton", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 8);


//Ariana Grande
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-5-20", 6);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-2-8", 6);


INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 242, "EDM house", 9);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", 156, "Pop, R&B", 10);


//Bruno Mars
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016-11-18", 7);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-01-20", 7);


INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 207, "Funk, disco, R&B", 11);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 152, "Pop", 12);









-- Use dot notation if there are fields that has similar label/name.
SELECT name, album_title, date_released, song_name, length, genre FROM artists
		JOIN albums ON artists.id = albums.artist_id
        JOIN songs ON albums.id = songs.album_id;


SELECT full_name, datetime_created, song_name, length, genre, album_title FROM users
	JOIN playlists ON users.id = playlists.user_id
    JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
    JOIN songs ON playlists_songs.song_id =songs.id
    JOIN albums ON songs.album_id = albums.id;